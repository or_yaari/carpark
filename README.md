# Car Park

This is the car park task for ubitricity made by Or Yaari.

## API

This task has 3 API calls you could use

plug a car - plug a car to a charging point, it requested a number of a charging point (1-10):
'http://localhost:8080/carPark/plug/{id}'

unplug a car - unplug a car from a charging point, it requested a number of a charging point (1-10):
'http://localhost:8080/carPark/unplug/{id}'

report - get a report of the charging points:
'http://localhost:8080/carPark/report'