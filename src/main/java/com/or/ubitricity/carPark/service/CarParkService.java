package com.or.ubitricity.carPark.service;

import com.or.ubitricity.carPark.model.CarPark;
import com.or.ubitricity.carPark.model.ChargingPoint;
import com.or.ubitricity.carPark.model.ChargingType;
import org.springframework.stereotype.Service;

@Service
public class CarParkService {

    private static final int MAX_AMPER_CONSUMPTION = 100;
    private CarPark carPark;

    public CarParkService() {
        this.carPark = new CarPark();
    }

    public String plugCar(int chargingPointId) {
        String response = this.carPark.plugCar(chargingPointId);
        this.updateConsumption();
        return response;
    }

    public String unplugCar(int chargingPointId) {
        String response = this.carPark.unplugCar(chargingPointId);
        this.updateConsumption();
        return response;
    }

    public String report() {
        return this.carPark.report();
    }


    private void updateConsumption() {
        int currentConsumptionRemains = MAX_AMPER_CONSUMPTION;
        int remainingPoints = carPark.getOccupiedPoints().size();
        for (ChargingPoint chargingPoint : carPark.getOccupiedPoints()) {
            if (currentConsumptionRemains / remainingPoints >= ChargingType.FAST.getRate()) {
                chargingPoint.setChargingType(ChargingType.FAST);
                currentConsumptionRemains -= ChargingType.FAST.getRate();
            } else {
                chargingPoint.setChargingType(ChargingType.SLOW);
                currentConsumptionRemains -= ChargingType.SLOW.getRate();
            }
            remainingPoints--;
        }
    }
}
