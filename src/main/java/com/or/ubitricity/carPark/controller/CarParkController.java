package com.or.ubitricity.carPark.controller;

import com.or.ubitricity.carPark.service.CarParkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.or.ubitricity.carPark.model.CarPark.MAX_CHARGING_POINTS;

@Controller
public class CarParkController {

    @Autowired
    private CarParkService carParkService;

    @RequestMapping(path = "/carPark/report", produces = "text/plain")
    public ResponseEntity<String> report() {
        return ResponseEntity.ok(carParkService.report());
    }

    @RequestMapping(path = "/carPark/plug/{id}")
    public ResponseEntity<String> plug(@PathVariable("id") int id ) {
        if (idValidator(id)) {
            return ResponseEntity.ok(carParkService.plugCar(id));
        } else {
            return ResponseEntity.badRequest().body("Id : " + id + " is not valid");
        }
    }

    @RequestMapping(path = "/carPark/unplug/{id}")
    public ResponseEntity<String> unplug(@PathVariable("id") int id ) {
        if (idValidator(id)) {
            return ResponseEntity.ok(carParkService.unplugCar(id));
        } else {
            return ResponseEntity.badRequest().body("Id : " + id + " is not valid");
        }
    }

    private boolean idValidator(int id) {
        return id > 0 && id <= MAX_CHARGING_POINTS;
    }
}
