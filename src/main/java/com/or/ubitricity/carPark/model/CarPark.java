package com.or.ubitricity.carPark.model;

import java.util.LinkedList;

public class CarPark {
    public static final int MAX_CHARGING_POINTS = 10;

    private final ChargingPoint[] chargingPoints;
    private final LinkedList<ChargingPoint> occupiedPoints;

    public CarPark() {
        this.chargingPoints = new ChargingPoint[MAX_CHARGING_POINTS];
        this.occupiedPoints = new LinkedList<>();
        initializeChargingPoints();
    }

    private void initializeChargingPoints() {
        for (int i = 0; i < chargingPoints.length; i++) {
            chargingPoints[i] = new ChargingPoint("CP" + String.valueOf(i+1));
        }
    }

    public LinkedList<ChargingPoint> getOccupiedPoints() {
        return occupiedPoints;
    }

    public String plugCar(int chargingPointId) {
        ChargingPoint chargingPoint = chargingPoints[chargingPointId - 1];
        if (chargingPoint.isAvailable()) {
            chargingPoint.setAvailable(false);
            occupiedPoints.addLast(chargingPoint);
            return "A car was plugged to charging point " + chargingPointId;
        } else  {
            return "Charging point " + chargingPointId + " is already occupied";
        }
    }

    public String unplugCar(int chargingPointId) {
        ChargingPoint chargingPoint = chargingPoints[chargingPointId - 1];
        if (!chargingPoint.isAvailable()) {
            chargingPoint.setAvailable(true);
            occupiedPoints.remove(chargingPoint);
            return "A car was unplugged from charging point " + chargingPointId;
        } else  {
            return "Charging point " + chargingPointId + " is already available";
        }
    }

    public String report() {
        StringBuilder stringBuilder = new StringBuilder("````\nReport:\n");
        for (ChargingPoint chargingPoint: chargingPoints) {
            stringBuilder.append(chargingPoint.toString()).append("\n");
        }

        stringBuilder.append("```");

        return stringBuilder.toString();
    }

}
