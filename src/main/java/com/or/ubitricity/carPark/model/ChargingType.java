package com.or.ubitricity.carPark.model;

public enum ChargingType {
    NONE(0),
    SLOW(10),
    FAST(20);

    ChargingType(int rate) {
        this.rate = rate;
    }

    private int rate;

    public int getRate() {
        return rate;
    }
}
