package com.or.ubitricity.carPark.model;

public class ChargingPoint {
    private final String name;
    private boolean isAvailable;
    private ChargingType chargingType;

    public ChargingPoint(String name) {
        this.name = name;
        this.isAvailable = true;
        this.chargingType = ChargingType.NONE;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public void setChargingType(ChargingType chargingType) {
        this.chargingType = chargingType;
    }

    @Override
    public String toString() {
        if (isAvailable) {
            return name + " AVAILABLE";
        } else {
            return name + " OCCUPIED " + chargingType.getRate() + "A";
        }
    }
}
