package com.or.ubitricity.carPark;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.or.ubitricity.carPark"})
public class AppConfig {
}
