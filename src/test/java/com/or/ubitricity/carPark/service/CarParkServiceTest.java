package com.or.ubitricity.carPark.service;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.junit.Assert.*;

public class CarParkServiceTest {

    private CarParkService carParkService;

    @Before
    public void setUp() throws Exception {
        this.carParkService = new CarParkService();
    }

    @Test
    public void report() throws FileNotFoundException {
        this.carParkService.plugCar(1);
        this.carParkService.plugCar(4);
        this.carParkService.plugCar(6);
        this.carParkService.plugCar(2);
        this.carParkService.plugCar(3);
        this.carParkService.plugCar(9);
        String report = this.carParkService.report();
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("reportText.txt").getFile());
        Scanner scanner = new Scanner(file);
        StringBuilder reportFile = new StringBuilder();
        while (scanner.hasNext())
        {
            reportFile.append(scanner.nextLine());
            reportFile.append("\n");
        }
        assertEquals(reportFile.toString().trim(), report);
    }
}