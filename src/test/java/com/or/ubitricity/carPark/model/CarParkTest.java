package com.or.ubitricity.carPark.model;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class CarParkTest {

    private CarPark carPark;

    @Before
    public void setUp() throws Exception {
        this.carPark = new CarPark();
    }

    @Test
    public void getOccupiedPoints() {
        this.carPark.plugCar(5);
        assertEquals(1, this.carPark.getOccupiedPoints().size());
    }

    @Test
    public void plugCar() {
        String response = this.carPark.plugCar(5);
        assertEquals("A car was plugged to charging point 5", response);
    }

    @Test
    public void plugCar_givenAlreadyOccupiedCP_returnCPIsAlreadyOccupied() {
        this.carPark.plugCar(5);
        String response = this.carPark.plugCar(5);
        assertEquals("Charging point 5 is already occupied", response);
    }

    @Test
    public void unplugCar() {
        this.carPark.plugCar(5);
        String response = this.carPark.unplugCar(5);
        assertEquals("A car was unplugged from charging point 5", response);
    }

    @Test
    public void unplugCar_givenFreeCP_returnCPIsEmpty() {
        String response = this.carPark.unplugCar(5);
        assertEquals("Charging point 5 is already available", response);
    }
}