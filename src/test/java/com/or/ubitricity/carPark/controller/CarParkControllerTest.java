package com.or.ubitricity.carPark.controller;

import com.or.ubitricity.carPark.AppConfig;
import com.or.ubitricity.carPark.service.CarParkService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class CarParkControllerTest {

    @Autowired
    private CarParkController carParkController;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void plug_givenNegativeNumber_getBadResponse() {
        ResponseEntity<String> response = carParkController.plug(-5);
        assertEquals(BAD_REQUEST,response.getStatusCode());
        assertEquals("Id : -5 is not valid", response.getBody());
    }

    @Test
    public void plug_givenOverTheMaxNumber_getBadResponse() {
        ResponseEntity<String> response = carParkController.plug(100);
        assertEquals(BAD_REQUEST,response.getStatusCode());
        assertEquals("Id : 100 is not valid", response.getBody());
    }

    @Test
    public void unplug_givenZero_getBadResponse() {
        ResponseEntity<String> response = carParkController.plug(0);
        assertEquals(BAD_REQUEST,response.getStatusCode());
        assertEquals("Id : 0 is not valid", response.getBody());
    }
}